import React from 'react';
import BaseLogin from './BaseLogin';
import {connect} from 'react-redux';
import * as actions from '../../store/actions';

const baseLogin = Component => {
     class ComponentBaseLogin extends React.Component{
        componentDidMount(){
            const {logado} = this.props;
            console.log(logado);
        }

        componentDidMount(nextProps){
            console.log(nextProps);
        }
        
        render(){
            return(
                <BaseLogin>
                    <Component { ...this.props} />
                </BaseLogin>
            );
        }
    }

    const mapStateToProps = state => ({
        logado: state.auth.logado,
        usuario: state.auth.usuario
    });

    return connect(mapStateToProps, actions)(ComponentBaseLogin)
}

export default baseLogin;