import axios from 'axios';
import md5 from 'md5';
//import {api} from '../../config';
//import { saveToke } from './localStorage';
import errorHandling from './errorHandling';
import {LOGIN_USER, LOGOUT_USER } from './types';

const public_key = '98416da7cf71ad79b538280586e6e53b';
const private_key = '0bfc39c9a0b0fa7e4a77d5aecbb283656eda1b85';

const time = Number(new Date());
const hash = md5(time + private_key + public_key);

export const handleLogin = ({private_key, public_key}, callback) => {
    return function(dispatch){ //enviar os dados 
        axios.get(
            `http://gateway.marvel.com/v1/public/characters?ts=${time}&apikey=${public_key}&hash=${hash}`,
            )
                .then((response) => {
                    console.log(response.data.data);
                    //saveToke(response.data.data);
                    dispatch({type: LOGIN_USER, payload: response.data.data});
                })
                .catch((err) => callback(errorHandling(err)))
                dispatch({type: LOGOUT_USER});
    }

}

export const handleLogout = () =>{
    dispatch({type: LOGOUT_USER});
    return {type: LOGOUT_USER};
}