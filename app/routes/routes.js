import React from 'react';
import { BrowserRouter, Router, Switch, Route } from 'react-router-dom';

import { Provider } from 'react-redux';
import store from '../store/store.js';

import { history } from '../history';
import PrivateRoute from './PrivateRoute';

import Login from '../pages/Login';
import RecuperarSenha from '../pages/RecuperarSenha';
import Dashboard from '../pages/Dashboard';
import Perfil from '../pages/Hero';

import baseLogin from '../containers/login';
import baseDashboard from '../containers/dashboard';

export default function Routes() {
    return (
        <Provider store={store}>
            <Router history={history}>
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={baseLogin(Login)} />
                        <Route path="/recuperar-senha" exact component={baseLogin(RecuperarSenha)} />
                        <PrivateRoute path="/dashboard" exact component={baseDashboard(Dashboard)} />
                        <PrivateRoute path="/hero" exact component={baseDashboard(Perfil)} />
                    </Switch>
                </BrowserRouter>
            </Router>
        </Provider>
    );
}