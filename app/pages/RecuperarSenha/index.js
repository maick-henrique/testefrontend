import React from 'react';
import { Form, FormGroup, Input, Button } from 'reactstrap';

export default function RecuperarSenha() {
    return (
        <>
        <div className="container-login">
            <div className="login card shadow">
                <Form className="form-signin text-center">
                    <img className="mb-4" src="images/logo_celke.png" alt="Celke" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Recuperar senha</h1>

                    <FormGroup>
                        <Input type="email" name="private_key" id="private_key" placeholder="private_key" />
                    </FormGroup>

                    <FormGroup>
                        <Input type="text" name="public_key" id="public_key" placeholder="public_key" />
                    </FormGroup>

                    <Button color="primary btn-block">Acessar</Button>
                </Form>
            </div>
        </div>
        </>
    )
}