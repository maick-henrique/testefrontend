import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

class Hero extends Component {

    state = {
        hero: "",
        firstName: ""
    }

    componentDidMount() {
        const { hero } = this.props;
        this.setState({ hero: hero });

        var name = "Usuário";
        if(hero){
            if(hero.name){
                [firstName] = hero.name.split(' ');
            }
        }
        this.setState({firstName: firstName});
    }
    render() {
        const { name, description, modified } = this.state.usuario;
        const { firstName } = this.state;
        return (
            <>
                <div className="d-flex">
                    <div className="mr-auto p-2">
                        <h2 className="display-4 titulo">Marvel - Heróis</h2>
                    </div>
                </div><hr />
                <dl className="row">
                    <dt className="col-sm-3">Nome</dt>
                    <dd className="col-sm-9">{name}
                        <img src={url} alt={firstName} width="150" height="150" />
                    </dd>

                    <dt className="col-sm-3">Descrição</dt>
                    <dd className="col-sm-9">{description}</dd>

                    <dt className="col-sm-3">Última Atualização</dt>
                    <dd className="col-sm-9">{modified}</dd>
                </dl>
            </>
        )
    }
}

const mapStateToProps = state => ({
    hero: state.auth.hero
})

export default connect(mapStateToProps, actions)(Hero);