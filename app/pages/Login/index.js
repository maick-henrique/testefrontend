import React from 'react';
import { Form, FormGroup, Input, Button } from 'reactstrap';
import AlertDanger from '../../components/AlertDanger';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

class Login extends Component {

    state = {
        private_key: "",
        public_key: "",
        error: ""
    }

    onChangeInput = (field, ev) => {
        this.setState({ [field]: ev.target.value});
    }

    handleLogin(){
        const { private_key, public_key} = this.state;

        if(!this.validate()) return;
        this.props.handleLogin({private_key, public_key}, (err) => {
            this.setState({error: {message: err.error.message}})
        })
    }

    validate(){
        const { private_key, public_key } = this.state;
        if(!private_key)return this.setState({error: { message: "Insira a Private Key"}})
        if(!public_key)return this.setState({error: { message: "Insira a Public Key"}})

        return true;
    }

    render(){
        const { private_key, public_key, error} = this.state;

        return (
            <>
            <div className="container-login">
                <div className="login card shadow">
                    <Form className="form-signin text-center">
                        <h1 className="h3 mb-3 font-weight-normal">Dados de acesso</h1>
                        <AlertDanger errors={error}/>

                        <FormGroup>
                            <Input 
                            type="private_key"
                            value={private_key}
                            name="private_key"
                            id="private_key"
                            placeholder="private_key" 
                            onChange={(ev) => this.onChangeInput("private_key", ev)} />
                        </FormGroup>

                        <FormGroup>
                            <Input 
                            type="public_key" 
                            value={public_key}
                            name="public_key"
                            id="public_key" 
                            placeholder="public_key"
                            onChange={(ev) => this.onChangeInput("public_key", ev)} />
                        </FormGroup>

                        <Button color="primary btn-block"
                        size="lg"
                        onClick={() => this.handleLogin()}>Acessar</Button>
                    </Form>
                </div>
            </div>
            </>
        )
        }
}

export default connect (null, actions) (Login);