# README #

Application to consume the Marvel api and bring up the names, descriptions and modification date of the characters.

### Teste Front-End Printi ###


The platform was developed using React, React-router, Redux, BabelJS and without using create-react-app.



A few commands for installing packages were used
npm i --save react react-dom
npm i --save-dev @babel/core @babel/preset-env @babel/preset-react babel-loader css-loader html-webpack-plugin style-loader webpack webpack-cli webpack-dev-server



The platform is fully responsive and the code correctly handles the keys to link with the marvel api.

